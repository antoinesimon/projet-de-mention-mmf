import matplotlib.pyplot as plt
import time

import numpy as np
from scipy.integrate import quad
from scipy.special import erf, erfi
import itertools

from joblib import Parallel, delayed


def exp_polynomial_func(coefficients):
    def f(x):
        return np.exp(np.array([lambda_k * x**k for k, lambda_k in enumerate(coefficients)]).sum())
    return f

def compute_spatial_momentum(f, i, x_0, x_1):
    """
    Args :
        - f : function from real numbers to reals numbers
        - i : order of the momentum
        - x_0, x_1 : boundaries
    
    Returns :
        The i-th momentum of f on the interval [x_0, x_1]
    """
    def f_m(x):
        return f(x) * x**i
    return quad(f_m, x_0, x_1)[0]

def compute_momentums_degree_one(a, b, i, x_0, x_1):
    """
    Args :
        - a, b : coefficients of the polynomial in exp(-b - a*x)
        - i : maximum order of the momentum
        - x_0, x_1 : boundaries
    
    Returns :
        The 0-th to i-th momentum of exp(-b - a*x) on the interval [x_0, x_1]
    """
    dx = abs(x_1 - x_0)

    if a == 0.:
        return tuple([dx*np.exp(-b)/(k+1)*(1/2**(k+1) - (-1)**(k+1)/2**(k+1)) for k in range(i+1)])

    e_i_p1, e_i_m1 = np.exp(-b - a*x_1), np.exp(-b - a*x_0)
    inv_dx = 1./abs(x_1 - x_0)
    inv_a = -1./a
    m_0 = inv_a * (e_i_p1 - e_i_m1)
    if i == 0:
        return m_0
    else:
        m_list = [m_0]
        for j in range(1, i+1):
            e_i_p1, e_i_m1 = e_i_p1/2, -e_i_m1/2
            m_list.append(inv_a * (e_i_p1 - e_i_m1 - j * inv_dx * m_list[-1]))
        return tuple(m_list)

def compute_momentums_degree_two(a, b, c, i, x_0, x_1):
    """
    Args :
        - a, b, c : coefficients of the polynomial in exp(-c - b*x - a*x**2)
        - i : maximum order of the momentum
        - x_0, x_1 : boundaries
    
    Returns :
        The 0-th to i-th momentum of exp(-c - b*x - a*x**2) on the interval [x_0, x_1]
    """
    if a > 0:
        m_0 = 0.5 * np.sqrt(np.pi/a) * np.exp(-c + b**2/(4*a)) * (erf(np.sqrt(a)*x_1+b/2/np.sqrt(a)) - erf(np.sqrt(a)*x_0+b/2/np.sqrt(a)))
    else:
        m_0 = 0.5 * np.sqrt(np.pi/(-a)) * np.exp(-c + b**2/(4*a)) * (erfi(np.sqrt(-a)*x_1-b/2/np.sqrt(-a)) - erfi(np.sqrt(-a)*x_0-b/2/np.sqrt(-a)))
    if i == 0:
        return m_0

    x_i = 0.5 * (x_0 + x_1)
    inv_dx = 1./abs(x_1 - x_0)
    m_1 = -inv_dx * (0.5/a*(np.exp(-c-b*x_1-a*x_1**2) - np.exp(-c-b*x_0-a*x_0**2)) + (b/a/2+x_i)*m_0)
    if i == 1:
        return (m_0, m_1)

    m_list = [m_0, m_1]
    for j in range(2, i+1):
        m_list.append(-inv_dx/a/2/2**(j-1) * (np.exp(-c-b*x_1-a*x_1**2) - (-1)**(j-1)*np.exp(-c-b*x_0-a*x_0**2)) + (j-1)*inv_dx*inv_dx/a/2*m_list[-2] - inv_dx*(b/a/2+x_i)*m_list[-1])

    return tuple(m_list)


def write_out(x_list, y_list, z_iterator, filename="ouput_0"):
    with open(filename+'.data', 'w') as f:
        f.writelines((str(x_list[i]) + ', ' + str(y_list[i]) + ', ' + str(next(z_iterator)) + '\n' for i in range(len(x_list))))

def repartition_2_parallelized(limit=15., N=40):
    R = np.linspace(-limit, limit, N)

    RR = [R[:N//4], R[N//4:N//2], R[N//2:3*N//4], R[3*N//4:]]

    def compute_momentums(Lambda_0, Lambda_1, Lambda_2, i):
        m_0 = 0.
        m_1_list = []
        m_2_list = []

        coefficients_iterator = itertools.product(Lambda_0, Lambda_1, Lambda_2)

        for coefficients in coefficients_iterator:
            polynomial = exp_polynomial_func(coefficients)
            m_0 = compute_spatial_momentum(polynomial, 0, -0.5, 0.5)
            m_1_list.append(compute_spatial_momentum(polynomial, 1, -0.5, 0.5)/m_0)
            m_2_list.append(compute_spatial_momentum(polynomial, 2, -0.5, 0.5)/m_0)

        # write_out(m_1_list, m_2_list, itertools.product(Lambda_0, Lambda_1, Lambda_2), filename="output_"+str(i))

        return (m_1_list, m_2_list)

    results = Parallel(n_jobs=4)(delayed(compute_momentums)(RR[i], R, R, i) for i in range(4))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    for (m_1, m_2) in results:
        ax.scatter(m_1, m_2, marker=',', s=0.2, color='blue')

    fig.savefig("momentum_test_without_formulas.png", dpi=200)

def repartition_2_parallelized2(limit=15., N=40):
    R = np.linspace(-limit, limit, N)

    RR = [R[:N//4], R[N//4:N//2], R[N//2:3*N//4], R[3*N//4:]]

    def compute_momentums(Lambda_0, Lambda_1, Lambda_2, i):
        m_0, m_1, m_2 = 0., 0., 0.
        m_1_list1, m_1_list2 = [], []
        m_2_list1, m_2_list2 = [], []

        coefficients_iterator = itertools.product(Lambda_0, Lambda_1, Lambda_2)

        for coefficients in coefficients_iterator:
            if coefficients[0] == 0.:
                m_0, m_1, m_2 = compute_momentums_degree_one(coefficients[1], coefficients[2], 2, -0.5, 0.5)
                m_1_list1.append(m_1/m_0)
                m_2_list1.append(m_2/m_0)
            else:
                m_0, m_1, m_2 = compute_momentums_degree_two(coefficients[0], coefficients[1], coefficients[2], 2, -0.5, 0.5)
                m_1_list2.append(m_1/m_0)
                m_2_list2.append(m_2/m_0)

        # write_out(m_1_list, m_2_list, itertools.product(Lambda_0, Lambda_1, Lambda_2), filename="output_"+str(i))

        return (m_1_list1, m_1_list2, m_2_list1, m_2_list2)

    results = Parallel(n_jobs=4)(delayed(compute_momentums)(RR[i], R, R, i) for i in range(4))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    for (m_11, m_12, m_21, m_22) in results:
        ax.scatter(m_12, m_22, marker=',', s=0.2, color='red')
        ax.scatter(m_11, m_21, marker=',', s=0.2, color='blue')

    fig.savefig("momentum_test_91.png", dpi=200)

def repartition_3_parallelized(limit=15., N=40):
    R = np.linspace(-limit, limit, N)

    RR = [R[:N//4], R[N//4:N//2], R[N//2:3*N//4], R[3*N//4:]]

    def compute_momentums(Lambda_0, Lambda_1, Lambda_2, lambda_3, i):
        m_0 = 0.
        m_1_list = []
        m_2_list = []

        coefficients_iterator = itertools.product(Lambda_0, Lambda_1, Lambda_2, lambda_3)

        for coefficients in coefficients_iterator:
            polynomial = exp_polynomial_func(coefficients)
            m_0 = compute_spatial_momentum(polynomial, 0, -0.1, 0.1)
            m_1_list.append(compute_spatial_momentum(polynomial, 1, -0.1, 0.1)/m_0)
            m_2_list.append(compute_spatial_momentum(polynomial, 2, -0.1, 0.1)/m_0)

        write_out(m_1_list, m_2_list, itertools.product(Lambda_0, Lambda_1, Lambda_2, lambda_3), filename="output_"+str(i))

        return (m_1_list, m_2_list)

    results = Parallel(n_jobs=4)(delayed(compute_momentums)(RR[i], R, R, R, i) for i in range(4))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    for (m_1, m_2) in results:
        ax.scatter(m_1, m_2, marker=',', s=0.2, color='blue')

    fig.savefig("momentum_test8.png", dpi=200)

t = time.time()
repartition_2_parallelized2(15., 41)
# repartition_2_parallelized(15., 21)
print(time.time() - t)

# print(compute_spatial_momentum(exp_polynomial_func([-1., -2., -3.]), 0, -0.5, 0.5))
# print(compute_spatial_momentum(exp_polynomial_func([-1., -2., -3.]), 1, -0.5, 0.5))
# print(compute_spatial_momentum(exp_polynomial_func([-1., -2., -3.]), 2, -0.5, 0.5))
# print(compute_momentums_degree_two(3., 2., 1., 2, -0.5, 0.5))

# print(compute_spatial_momentum(exp_polynomial_func([1., 1., -1.]), 0, -0.5, 0.5))
# print(compute_spatial_momentum(exp_polynomial_func([1., 1., -1.]), 1, -0.5, 0.5))
# print(compute_spatial_momentum(exp_polynomial_func([1., 1., -1.]), 2, -0.5, 0.5))
# print(compute_momentums_degree_two(1., -1., -1., 2, -0.5, 0.5))