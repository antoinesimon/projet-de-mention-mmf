import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

from scipy.integrate import quad



###########################################################
#                                                         #
#  First we need a function to properly plot our results  #
#                                                         #
###########################################################

def plot_anim(phi, x_0=0., x_1=1., t_ini=0., t_end=1.,
            duration=8, frames_per_second=15,
            limits_override=None, phi_name='phi', phi_name_ini='phi_ini', phi_name_fin='phi_fin'):

    fig, ax = plt.subplots()
    t_data = np.linspace(t_ini, t_end, np.size(phi, 0))
    x_data = np.linspace(x_0, x_1, np.size(phi, 1))

    # set the min and maximum values of the plot, to scale the axis, can be overriden by user
    m = min(0, np.min(phi))
    M = np.max(phi) + 1
    limits = [m, M]
    if limits_override is not None and type(limits_override) == list and len(limits_override) == 2:
        limits = limits_override

    # set the x_data axis
    ax.set(xlim=[x_0, x_1], ylim=limits, xlabel='x', ylabel=phi_name)

    # plot to be updated during the animation
    plot = ax.plot(x_data, phi[0,:], label=phi_name)[0]

    # plot of the initial distribution of phi for comparison
    ax.plot(x_data, phi[0,:], label=phi_name_ini)
    # plot of the final distribution of phi for comparison
    ax.plot(x_data, phi[-1,:], label=phi_name_fin)

    ax.legend(loc='upper right')

    def update(frame):
        print(frame)
        # get the data by linear interpolation
        t = frame / (duration * frames_per_second)
        phi_t = np.array([np.interp(t, t_data, phi[:,i]) for i in range(np.size(phi,1))])
        # update the plots
        plot.set_ydata(np.real(phi_t))

    ani = animation.FuncAnimation(fig=fig, func=update, frames=duration*frames_per_second, interval=1000/frames_per_second)
    return ani



########################################################################
#                                                                      #
# Then we need some functions to initialize the distributions we will  #
# use to check the algorithms                                          #
#                                                                      #
########################################################################

def gaussian_dist(x, mu=0.5, sigma=0.05, C=1., D=1.):
    return C + D/(sigma*np.sqrt(2*np.pi)) * np.exp(-0.5*(x-mu)**2/sigma**2)
vec_gaussian_dist = np.vectorize(gaussian_dist, excluded=['mu', 'sigma', 'C', 'D'])

def step_dist(x, height=1., x_c=0.5):
    return height if x <= x_c else 0.
vec_step_dist = np.vectorize(step_dist, excluded=['height', 'x_c'])


def ini_rho_gauss(x_0=0., x_1=1., nK=600, mu=0.5, sigma=0.05, C=1., D=1.):
    rho = []
    lengthK = (x_1 - x_0)/nK

    def gauss(x):
        return gaussian_dist(x, mu=mu, sigma=sigma, C=C, D=D)

    for i in range(nK):
        rho.append(quad(gauss, i*lengthK, (i+1)*lengthK)[0])
    return np.array(rho)/lengthK

def ini_rho_step(x_0=0., x_1=1., nK=600, height=1., x_c=0.5):
    X = np.linspace(x_0, x_1, nK)
    return vec_step_dist(X, height=height, x_c=x_c)

def ini_rho_u_shock(x_0=0., x_1=1., nK=600):
    X = np.linspace(x_0, x_1, nK)
    rho_left, rho_right = vec_gaussian_dist(X[:], mu=0.2, C=0.), vec_gaussian_dist(X[:], mu=0.8, C=0.)
    rho = 0.5 * (rho_left + rho_right)
    
    u_left, u_right = np.ones((nK//2,)), -np.ones((nK-nK//2,))
    u = np.concatenate((u_left, u_right))
    
    return (rho, u)



#########################################
#                                       #
#  Some usefull tools                   #
#    - to handle zero density           #
#    - to prepare slope limiters        #
#    - to integrate using explicit RK3  #
#                                       #
#########################################

def inverse_handle_zero(a, tol=1e-8):
    # return 1/a
    return 1/max(a, tol)
    if abs(a) > tol:
        return 1/a
    else:
        return 0
vec_inverse_handle_zero = np.vectorize(inverse_handle_zero, excluded=['tol'])

def slope_limiter(r, type="minMod"):
    if type == "minMod":
        return max(0., min(2/(1+r), 2*r/(1+r)))
    if type == "vanLeer":
        return 2*(r + abs(r)) / (1 + r)**2
    if type == "vanAlbada":
        return 2*r / (1 + r**2)
    if type == "superbee":
        return max(0, min(4*r/(1+r), 2/(1+r)), min(2*r/(1+r), 4/(1+r)))
    else:
        return 0
vec_slope_limiter = np.vectorize(slope_limiter, excluded=['type'])

def compute_r(rho_i__1, rho_i, rho_i_1):
    # notations :
    #        rho_i_1 corresponds to rho_{i+1}
    #        rho_i__1 corresponds to rho_{i-1}
    if rho_i - rho_i__1 == 0.:
        return 0.
    return (rho_i_1 - rho_i) / (rho_i - rho_i__1)
vec_compute_r = np.vectorize(compute_r)

def compute_slope(rho_i__1, rho_i, rho_i_1, lengthK, slope_limiter_type="minMod"):
    slope_0 = (rho_i_1 - rho_i__1)/lengthK/2
    return slope_limiter(compute_r(rho_i__1, rho_i, rho_i_1), type=slope_limiter_type) * slope_0
vec_compute_slope = np.vectorize(compute_slope, excluded=['lengthK', 'slope_limiter_type'])

def rk3(tini, tend, nt, yini, slope_ini, fcn, lengthK, u):

    dt = (tend-tini) / (nt-1)
    t = np.linspace(tini, tend, nt)

    yini = np.atleast_1d(yini)
    neq = yini.size

    y = np.zeros((neq, nt))
    y[:,0] = yini
    slope = np.zeros((neq))
    slope = slope_ini

    for it, tn  in enumerate(t[:-1]):
        yn = y[:,it]

        k1 = fcn(tn, yn, slope, lengthK, u)
        yn_k1 = yn + dt*(0.5*k1)
        slope[1:-1] = vec_compute_slope(yn_k1[:-2], yn_k1[1:-1], yn_k1[2:], lengthK)
        slope[0] = compute_slope(yn_k1[-1], yn_k1[0], yn_k1[1], lengthK)
        slope[-1] = compute_slope(yn_k1[-2], yn_k1[-1], yn_k1[0], lengthK)

        k2 = fcn(tn + 0.5*dt, yn_k1, slope, lengthK, u)
        yn_k2 = yn + dt*(-k1 + 2*k2)
        slope[1:-1] = vec_compute_slope(yn_k2[:-2], yn_k2[1:-1], yn_k2[2:], lengthK)
        slope[0] = compute_slope(yn_k2[-1], yn_k2[0], yn_k2[1], lengthK)
        slope[-1] = compute_slope(yn_k2[-2], yn_k2[-1], yn_k2[0], lengthK)

        k3 = fcn(tn + dt, yn + dt*(-k1 + 2*k2), slope, lengthK, u)
        y[:,it+1] = yn + (dt/6)*(k1+4*k2+k3)
        slope[1:-1] = vec_compute_slope(y[:-2,it+1], y[1:-1,it+1], y[2:,it+1], lengthK)
        slope[0] = compute_slope(y[-1,it+1], y[0,it+1], y[1,it+1], lengthK)
        slope[-1] = compute_slope(y[-2,it+1], y[-1,it+1], y[0,it+1], lengthK)

    nfev = 3*(nt-1)

    return (y, t, nfev)



########################################################################
#                                                                      #
#  ADVECTION : first usual finite volumes, then linear reconstruction  #
#                                                                      #
########################################################################

def advection(x_0=0., x_1=1., nK=600, t_ini=0., t_end=1., nt=801, u=1.):
    dt = (t_end - t_ini)/(nt-1)
    lengthK = 1/nK

    rho_ini_K = ini_rho_gauss(x_0=x_0, x_1=x_1, nK=nK)
    # rho_ini_K = ini_rho_step(x_0=x_0, x_1=x_1, nK=nK)

    rho_K = np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K
    flux = np.zeros(nK)

    for n in range(1, nt):
        if u >= 0:
            flux[:] = u * rho_K[n-1,:]
        else:
            flux[:-1] = u * rho_K[n-1,1:]
            flux[-1] = u * rho_K[n-1,0]

        rho_K[n,1:] = rho_K[n-1,1:] - dt/lengthK * (flux[1:] - flux[:-1])
        rho_K[n,0] = rho_K[n-1,0] - dt/lengthK * (flux[0] - flux[-1])
    
    return rho_K


def advection_linear(x_0=0., x_1=1., nK=600, t_ini=0., t_end=1., nt=801, u=1.):
    dt = (t_end - t_ini)/(nt-1)
    lengthK = 1/nK

    rho_ini_K = ini_rho_gauss(x_0=x_0, x_1=x_1, nK=nK, C=0.)
    # rho_ini_K = ini_rho_step(x_0=x_0, x_1=x_1, nK=nK)

    rho_K = np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K

    # the notation rho_i_1 corresponds to rho_{i+1} (same for the slope: slope_i_1 == slope_{i+1})
    def compute_flux(rho_i, slope_i, rho_i_1, slope_i_1, u):
        if u >= 0:
            return u * (rho_i + slope_i*lengthK/2)
        else:
            return u * (rho_i_1 - slope_i_1*lengthK/2)
    compute_flux_vec = np.vectorize(compute_flux, excluded=['u'])

    def fcn_linear_advection(t, rho_n, slope_n, lengthK, u):
        flux = compute_flux_vec(rho_n[:-1], slope_n[:-1], rho_n[1:], slope_n[1:], u)
        flux_end = compute_flux(rho_n[-1], slope_n[-1], rho_n[0], slope_n[0], u)
        return - 1/lengthK * np.concatenate(([flux[0] - flux_end], flux[1:] - flux[:-1], [flux_end - flux[-1]]))

    slope_K = np.zeros((nt, nK))
    slope_K[0,1:-1] = vec_compute_slope(rho_K[0,:-2], rho_K[0,1:-1], rho_K[0,2:], lengthK)
    slope_K[0,0] = compute_slope(rho_K[0,-1], rho_K[0,0], rho_K[0,1], lengthK)
    slope_K[0,-1] = compute_slope(rho_K[0,-2], rho_K[0,-1], rho_K[0,0], lengthK)

    # flux = np.zeros((nK-1,))
    # flux_end = 0.

    # for n in range(1, nt):
    #     flux = compute_flux_vec(rho_K[n-1,:-1], slope_K[n-1,:-1], rho_K[n-1,1:], slope_K[n-1,1:], u)
    #     flux_end = compute_flux(rho_K[n-1,-1], slope_K[n-1,-1], rho_K[n-1,0], slope_K[n-1,0], u)

    #     rho_K[n,1:-1] = rho_K[n-1,1:-1] - dt/lengthK * (flux[1:] - flux[:-1])
    #     rho_K[n,-1] = rho_K[n-1,-1] - dt/lengthK * (flux_end - flux[-1])
    #     rho_K[n,0] = rho_K[n-1,0] - dt/lengthK * (flux[0] - flux_end)

    #     slope_K[n,1:-1] = vec_compute_slope(rho_K[n,:-2], rho_K[n,1:-1], rho_K[n,2:], lengthK)
    #     slope_K[n,0] = compute_slope(rho_K[n,-1], rho_K[n,0], rho_K[n,1], lengthK)
    #     slope_K[n,-1] = compute_slope(rho_K[n,-2], rho_K[n,-1], rho_K[n,0], lengthK)

    # return rho_K

    return np.transpose(rk3(t_ini, t_end, nt, rho_ini_K, slope_K[0,:], fcn_linear_advection, lengthK, u)[0])



##################################################################
#                                                                #
#  PGD : first usual finite volumes, then linear reconstruction  #
#                                                                #
##################################################################

# We begin by defining a function to compute the flux
def compute_flux_PGD(u_i, u_i_1, rho_i, rho_i_1, tol=1e-8):
    r_i, r_i_1 = max(rho_i, tol), max(rho_i_1, tol)

    if u_i > 0 and u_i_1 >= 0:
        return (u_i*r_i, u_i**2*r_i)

    elif u_i > 0 and u_i_1 < 0:
        return (u_i*r_i + u_i_1*r_i_1, u_i**2*r_i + u_i_1**2*r_i_1)

    elif u_i <= 0 and u_i_1 >= 0:
        return (0., 0.)

    elif u_i <= 0 and u_i_1 < 0:
        return (u_i_1*r_i_1, u_i_1**2*r_i_1)
vec_compute_flux_PGD = np.vectorize(compute_flux_PGD)

def pgd(x_0=0., x_1=1., nK=600, t_ini=0., t_end=1., nt=801):
    dt = (t_end - t_ini)/(nt-1)
    lengthK = 1/nK

    rho_ini_K = ini_rho_gauss(x_0=x_0, x_1=x_1, nK=nK, C=0.0001)
    # rho_ini_K = ini_rho_step(x_0=x_0, x_1=x_1, nK=nK)
    u_ini_K = np.ones((nK,))

    # rho_ini_K, u_ini_K = ini_rho_u_shock(x_0=x_0, x_1=x_1, nK=nK)


    rho_K, u_K = np.zeros((nt, nK)), np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K
    u_K[0,:] = u_ini_K

    tol_density_nullity = 1e-8

    flux_pgd = (np.zeros((nK-1,)), np.zeros((nK-1,)))
    flux_pgd_end = (0., 0.)

    for n in range(1, nt):
        flux_pgd = vec_compute_flux_PGD(u_K[n-1,:-1], u_K[n-1,1:], rho_K[n-1,:-1], rho_K[n-1,1:])
        flux_pgd_end = compute_flux_PGD(u_K[n-1,-1], u_K[n-1,0], rho_K[n-1,-1], rho_K[n-1,0])

        rho_K[n,1:-1] = rho_K[n-1,1:-1] + dt/lengthK * (flux_pgd[0][:-1] - flux_pgd[0][1:])
        rho_K[n,-1] = rho_K[n-1,-1] + dt/lengthK * (flux_pgd[0][-1] - flux_pgd_end[0])
        rho_K[n,0] = rho_K[n-1,0] + dt/lengthK * (flux_pgd_end[0] - flux_pgd[0][0])

        u_K[n,1:-1] = vec_inverse_handle_zero(rho_K[n,1:-1], tol_density_nullity) * (rho_K[n-1,1:-1]*u_K[n-1,1:-1] + dt/lengthK * (flux_pgd[1][:-1] - flux_pgd[1][1:]))
        u_K[n,-1] = inverse_handle_zero(rho_K[n,-1], tol_density_nullity) * (rho_K[n-1,-1]*u_K[n-1,-1] + dt/lengthK * (flux_pgd[1][-1] - flux_pgd_end[1]))
        u_K[n,0] = inverse_handle_zero(rho_K[n,0], tol_density_nullity) * (rho_K[n-1,0]*u_K[n-1,0] + dt/lengthK * (flux_pgd_end[1] - flux_pgd[1][0]))
    
    return (rho_K, u_K)


def compute_flux_PGD_linear(u_i, u_i_1, rho_i, rho_i_1, slope_rho_i, slope_rho_i_1, lengthK):
    rho_i_right, rho_i_1_left = rho_i + slope_rho_i*lengthK/2, rho_i_1 - slope_rho_i_1*lengthK/2

    if u_i > 0 and u_i_1 >= 0:
        return (u_i*rho_i_right, u_i**2*rho_i_right)
    elif u_i > 0 and u_i_1 < 0:
        return (u_i*rho_i_right + u_i_1*rho_i_1_left, u_i**2*rho_i_right + u_i_1**2*rho_i_1_left)
    elif u_i <= 0 and u_i_1 >= 0:
        return (0, 0)
    elif u_i <= 0 and u_i_1 < 0:
        return (u_i_1*rho_i_1_left, u_i_1**2*rho_i_1_left)

vec_compute_flux_PGD_linear = np.vectorize(compute_flux_PGD_linear, excluded=['lengthK'])

def pgd_linear(x_0=0., x_1=1., nK=600, t_ini=0., t_end=1., nt=801):
    dt = (t_end - t_ini)/(nt-1)
    lengthK = 1/nK

    # rho_ini_K = ini_rho_gauss(x_0=x_0, x_1=x_1, nK=nK, C=0.)
    # rho_ini_K = ini_rho_step(x_0=x_0, x_1=x_1, nK=nK)
    # rho_ini_K = 0.001*np.ones((nK,)) + ini_rho_step(x_0=x_0, x_1=x_1, nK=nK)
    # u_ini_K = np.ones((nK,))

    rho_ini_K, u_ini_K = ini_rho_u_shock(x_0=x_0, x_1=x_1, nK=nK)

    rho_K, u_K = np.zeros((nt, nK)), np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K
    u_K[0,:] = u_ini_K

    tol_density_nullity = 1e-8

    slope_K = np.zeros((nt, nK))
    slope_K[0,1:-1] = vec_compute_slope(rho_K[0,:-2], rho_K[0,1:-1], rho_K[0,2:], lengthK)
    slope_K[0,0] = compute_slope(rho_K[0,-1], rho_K[0,0], rho_K[0,1], lengthK)
    slope_K[0,-1] = compute_slope(rho_K[0,-2], rho_K[0,-1], rho_K[0,0], lengthK)

    flux_pgd_linear = (np.zeros((nK-1,)), np.zeros((nK-1,)))
    flux_pgd_linear_end = (0., 0.)

    for n in range(1, nt):
        flux_pgd_linear = vec_compute_flux_PGD_linear(u_K[n-1,:-1], u_K[n-1,1:], rho_K[n-1,:-1], rho_K[n-1,1:], slope_K[n-1,:-1], slope_K[n-1,1:], lengthK)
        flux_pgd_linear_end = compute_flux_PGD_linear(u_K[n-1,-1], u_K[n-1,0], rho_K[n-1,-1], rho_K[n-1,0], slope_K[n-1,-1], slope_K[n-1,0], lengthK)

        rho_K[n,1:-1] = rho_K[n-1,1:-1] + dt/lengthK * (flux_pgd_linear[0][:-1] - flux_pgd_linear[0][1:])
        rho_K[n,-1] = rho_K[n-1,-1] + dt/lengthK * (flux_pgd_linear[0][-1] - flux_pgd_linear_end[0])
        rho_K[n,0] = rho_K[n-1,0] + dt/lengthK * (flux_pgd_linear_end[0] - flux_pgd_linear[0][0])

        u_K[n,1:-1] = vec_inverse_handle_zero(rho_K[n,1:-1], tol_density_nullity) * (rho_K[n-1,1:-1]*u_K[n-1,1:-1] + dt/lengthK * (flux_pgd_linear[1][:-1] - flux_pgd_linear[1][1:]))
        u_K[n,-1] = inverse_handle_zero(rho_K[n,-1], tol_density_nullity) * (rho_K[n-1,-1]*u_K[n-1,-1] + dt/lengthK * (flux_pgd_linear[1][-1] - flux_pgd_linear_end[1]))
        u_K[n,0] = inverse_handle_zero(rho_K[n,0], tol_density_nullity) * (rho_K[n-1,0]*u_K[n-1,0] + dt/lengthK * (flux_pgd_linear_end[1] - flux_pgd_linear[1][0]))

        slope_K[n,1:-1] = vec_compute_slope(rho_K[n,:-2], rho_K[n,1:-1], rho_K[n,2:], lengthK)
        slope_K[n,0] = compute_slope(rho_K[n,-1], rho_K[n,0], rho_K[n,1], lengthK)
        slope_K[n,-1] = compute_slope(rho_K[n,-2], rho_K[n,-1], rho_K[n,0], lengthK)
    
    return (rho_K, u_K)



#########################################################
#                                                       #
#  Then we enter the testing zone which may be messier  #
#  Some UI needs to be enhanced !                       #   
#                                                       #
#########################################################

rho_name='$\\rho_{simu}$'
rho_name_ini='$\\rho_{simu}^{ini}$'
rho_name_fin='$\\rho_{simu}^{fin}$'
u_name='$u_{simu}$'
u_name_ini='$u_{simu}^{ini}$'

# rho_K_advection = advection()
# ani1 = plot_anim(rho_K_advection, phi_name=rho_name, phi_name_ini=rho_name_ini)
# ani1.save("advection_test.gif")

rho_K_linear_advection = advection_linear(nK=400, nt=901, u=1.)
ani2 = plot_anim(rho_K_linear_advection, phi_name=rho_name, phi_name_ini=rho_name_ini, phi_name_fin=rho_name_fin)
ani2.save("advection_linear_testrk3_______.gif")


# rho_K_PGD, u_K_PGD = pgd(nK=500, nt=1201)
# # ani3 = plot_anim(rho_K_PGD, limits_override=[0, 15], phi_name=rho_name, phi_name_ini=rho_name_ini)
# ani3 = plot_anim(rho_K_PGD, phi_name=rho_name, phi_name_ini=rho_name_ini)
# ani3.save("pgd_test_void_and_limiter.gif")
# ani4 = plot_anim(u_K_PGD, phi_name=u_name, phi_name_ini=u_name_ini)
# ani4.save("pgd_test_void_and_limiter_u.gif")

# rho_K_linear_PGD, u_K_linear_PGD = pgd_linear(nK=500, nt=1101)
# ani5 = plot_anim(rho_K_linear_PGD, limits_override=[0, 15], phi_name=rho_name, phi_name_ini=rho_name_ini, phi_name_fin=rho_name_fin)
# ani5.save("pgd_linear_test_shock3.gif")
# ani6 = plot_anim(u_K_linear_PGD, phi_name=u_name, phi_name_ini=u_name_ini)
# ani6.save("pgd_linear_test_shock3_u.gif")
