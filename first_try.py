import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

from scipy.integrate import quad


t_end = 1
nt = 801
dt = t_end/(nt-1)

nK = 600
lengthK = 1/nK

v = 1

T = np.linspace(0, t_end, nt)
# x_i is the left interface of volume K_i which length is 1/nK and the first and last x_i are the same according to periodic BC
X = np.linspace(0, 1, nK+1)
# We have that F_i is the flux from volume K_i to volume K_{i+1} and with periodic BC we have that K_{nK-1} is the flux from last volume to the first one
F = np.zeros(nK)


def plot_rho(rho_simu, X, duration=8, frames_per_second=15, limits_override=None):
    fig, ax = plt.subplots()
    t_data = np.linspace(0, t_end, np.size(rho_simu, 0))
    x_data = X.copy()

    # set the min and maximum values of the plot, to scale the axis
    m = min(0, np.min(rho_simu))
    M = np.max(rho_simu) + 1
    
    limits = [m, M]
    if limits_override is not None and type(limits_override) == list and len(limits_override) == 2:
        limits = limits_override

    # set the axis once and for all
    ax.set(xlim=[0, 1], ylim=limits, xlabel='x', ylabel='$\\rho_{simu}$')

    # plot to be updated during the animation
    plot = ax.plot(x_data, rho_simu[0,:], label='$\\rho_{simu}$')[0]

    # plot of the initial distribution of rho for comparison
    ax.plot(x_data, rho_simu[0,:], label='$\\rho_{simu}^{ini}$')

    ax.legend(loc='upper right')

    # define update function as an internal function (that can access the variables defined before)
    # will be called with frame=0...(duration*frames_per_second)-1
    def update(frame):
        print(frame)
        # get the data by linear interpolation
        t = frame / (duration * frames_per_second)
        rho_t = np.array([np.interp(t, t_data, rho_simu[:,i]) for i in range(np.size(rho_simu,1))])
        # update the plots
        plot.set_ydata(np.real(rho_t))

    ani = animation.FuncAnimation(fig=fig, func=update, frames=duration*frames_per_second, interval=1000/frames_per_second)
    return ani

def plot_u(u_simu, X, duration=8, frames_per_second=15, limits_override=None):
    fig, ax = plt.subplots()
    t_data = np.linspace(0, t_end, np.size(u_simu, 0))
    x_data = X.copy()

    # set the min and maximum values of the plot, to scale the axis
    m = min(0, np.min(u_simu))
    M = np.max(u_simu) + 1
    
    limits = [m, M]
    if limits_override is not None and type(limits_override) == list and len(limits_override) == 2:
        limits = limits_override

    # set the axis once and for all
    ax.set(xlim=[0, 1], ylim=limits, xlabel='x', ylabel='$u_{simu}$')

    # plot to be updated during the animation
    plot = ax.plot(x_data, u_simu[0,:], label='$u_{simu}$')[0]

    # plot of the initial distribution of u for comparison
    ax.plot(x_data, u_simu[0,:], label='$u_{simu}^{ini}$')

    ax.legend(loc='upper right')

    # define update function as an internal function (that can access the variables defined before)
    # will be called with frame=0...(duration*frames_per_second)-1
    def update(frame):
        print(frame)
        # get the data by linear interpolation
        t = frame / (duration * frames_per_second)
        u_t = np.array([np.interp(t, t_data, u_simu[:,i]) for i in range(np.size(u_simu,1))])
        # update the plots
        plot.set_ydata(np.real(u_t))

    ani = animation.FuncAnimation(fig=fig, func=update, frames=duration*frames_per_second, interval=1000/frames_per_second)
    return ani


def gauss(x):
    mu = 0.5    
    sigma = 0.05
    C = 1
    D = 1
    return C + D/(sigma*np.sqrt(2*np.pi)) * np.exp(-0.5*(x-mu)**2/sigma**2)

def ini():
    rho = []
    for i in range(nK):
        rho.append(quad(gauss, X[i], X[i+1])[0])
    return np.array(rho)/lengthK

def ini_step():
    return np.concatenate((np.ones(nK//2), np.zeros(nK-nK//2)))


def finite_vol_for_advection():
    # rho_ini_K = ini()
    rho_ini_K = ini_step()

    rho_K = np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K

    # for n in range(1, nt):
    #     for i in range(nK):
    #         if v > 0:
    #             F[i] = v * rho_K[n-1,i]
    #         else:
    #             F[i] = v * rho_K[n-1,(i+1)%nK]

    #     for j in range(nK):
    #         rho_K[n,j] = rho_K[n-1,j] - dt/lengthK * (F[j] - F[(j-1)%nK])

    for n in range(1, nt):
        if v >= 0:
            F[:] = v * rho_K[n-1,:]
        else:
            F[:-1] = v * rho_K[n-1,1:]
            F[-1] = v * rho_K[n-1,0]

        rho_K[n,1:] = rho_K[n-1,1:] - dt/lengthK * (F[1:] - F[:-1])
        rho_K[n,0] = rho_K[n-1,0] - dt/lengthK * (F[0] - F[-1])
    
    return rho_K

def linear_finite_vol_advection():
    def compute_flux(rho_i, slope_i, rho_i_1, slope_i_1, v):
        if v >= 0:
            return v * (rho_i + slope_i*lengthK/2)
        else:
            return v * (rho_i_1 - slope_i_1*lengthK/2)
    compute_flux_vec = np.vectorize(compute_flux, excluded=['v'])

    def compute_slope(rho_i__1, rho_i, rho_i_1, slope_limit=10):
        slope_1 = (rho_i_1 - rho_i__1)/lengthK
        if abs(slope_1) > slope_limit:
            return slope_limit if slope_1 > 0 else -slope_limit
        elif rho_i - slope_1*lengthK/2 >= 0 and rho_i + slope_1*lengthK/2 >= 0:
            return slope_1
        elif slope_1 >= 0:
            return rho_i/(lengthK/2)
        else:
            return -rho_i/(lengthK/2)
    compute_slope_vec = np.vectorize(compute_slope, excluded=['slope_limit'])

    rho_ini_K = ini()
    # rho_ini_K = ini_step()

    rho_K = np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K

    slope_K = np.zeros((nt, nK))
    # slope_K[0,1:-1] = (rho_K[0,2:] - rho_K[0,:-2]) / lengthK
    # slope_K[0,0] = (rho_K[0,1] - rho_K[0,-1]) / lengthK
    # slope_K[0,-1] = (rho_K[0,0] - rho_K[0,-2]) / lengthK
    slope_K[0,1:-1] = compute_slope_vec(rho_K[0,:-2], rho_K[0,1:-1], rho_K[0,2:])
    slope_K[0,0] = compute_slope(rho_K[0,-1], rho_K[0,0], rho_K[0,1])
    slope_K[0,-1] = compute_slope(rho_K[0,-2], rho_K[0,-1], rho_K[0,0])

    FLUX_0 = np.zeros((nK-1,))
    FLUX_end = 0.

    for n in range(1, nt):
        FLUX_0 = compute_flux_vec(rho_K[n-1,:-1], slope_K[n-1,:-1], rho_K[n-1,1:], slope_K[n-1,1:], v)
        FLUX_end = compute_flux(rho_K[n-1,-1], slope_K[n-1,-1], rho_K[n-1,0], slope_K[n-1,0], v)

        rho_K[n,1:-1] = rho_K[n-1,1:-1] - dt/lengthK * (FLUX_0[1:] - FLUX_0[:-1])
        rho_K[n,-1] = rho_K[n-1,-1] - dt/lengthK * (FLUX_end - FLUX_0[-1])
        rho_K[n,0] = rho_K[n-1,0] - dt/lengthK * (FLUX_0[0] - FLUX_end)

        # slope_K[n,1:-1] = (rho_K[n,2:] - rho_K[n,:-2]) / lengthK
        # slope_K[n,0] = (rho_K[n,1] - rho_K[n,-1]) / lengthK
        # slope_K[n,-1] = (rho_K[n,0] - rho_K[n,-2]) / lengthK
        slope_K[n,1:-1] = compute_slope_vec(rho_K[n,:-2], rho_K[n,1:-1], rho_K[n,2:])
        slope_K[n,0] = compute_slope(rho_K[n,-1], rho_K[n,0], rho_K[n,1])
        slope_K[n,-1] = compute_slope(rho_K[n,-2], rho_K[n,-1], rho_K[n,0])

    return rho_K


def num_flux_PGD(u_i, u_i_1, rho_i, rho_i_1):
    if u_i > 0 and u_i_1 >= 0:
        return (u_i*rho_i, u_i**2*rho_i)
    elif u_i > 0 and u_i_1 < 0:
        return (u_i*rho_i + u_i_1*rho_i_1, u_i**2*rho_i + u_i_1**2*rho_i_1)
    elif u_i <= 0 and u_i_1 >= 0:
        return (0, 0)
    elif u_i <= 0 and u_i_1 < 0:
        return (u_i_1*rho_i_1, u_i_1**2*rho_i_1)

num_flux_PGD_vec = np.vectorize(num_flux_PGD)

def inverse_handle_zero(x, tol):
    if abs(x) > tol:
        return 1/x
    else:
        return 0

inverse_handle_zero_vec = np.vectorize(inverse_handle_zero, excluded=['tol'])

def gaussian(x, mu=0.5, sigma=0.05, C=1, D=1):
    return C + D/(sigma*np.sqrt(2*np.pi)) * np.exp(-0.5*(x-mu)**2/sigma**2)

gaussian_vec = np.vectorize(gaussian, excluded=['mu', 'sigma', 'C', 'D'])

def ini_chock():
    rho_left, rho_right = gaussian_vec(X[:-1], mu=0.2), gaussian_vec(X[:-1], mu=0.8)
    rho = 0.5 * (rho_left + rho_right)
    
    u_l, u_r = np.ones((nK//2,)), -np.ones((nK-nK//2,))
    u = np.concatenate((u_l, u_r))
    
    return (rho, u)

def ini_chock_steps():
    rho = np.concatenate((np.ones(nK//4), np.zeros(nK-2*nK//4), np.ones(nK//4)))
    
    u_l, u_r = np.ones((nK//2,)), -np.ones((nK-nK//2,))
    u = np.concatenate((u_l, u_r))
    
    return (rho, u)

def finite_vol_for_pressureless_gas_dynamics():
    # rho_ini_K = ini()
    # rho_ini_K = 0.001*np.ones((nK,)) + ini_step()
    rho_ini_K = ini_step()
    u_ini_K = np.ones((nK,))

    # rho_ini_K, u_ini_K = ini_chock()
    # rho_ini_K, u_ini_K = ini_chock_steps()


    rho_K = np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K
    u_K = np.zeros((nt, nK))
    u_K[0,:] = u_ini_K

    tol_density_nullity = 1e-10

    FLUX_0 = (np.zeros((nK-1,)), np.zeros((nK-1,)))
    FLUX_end = (0., 0.)

    for n in range(1, nt):
        FLUX_0 = num_flux_PGD_vec(u_K[n-1,:-1], u_K[n-1,1:], rho_K[n-1,:-1], rho_K[n-1,1:])
        FLUX_end = num_flux_PGD(u_K[n-1,-1], u_K[n-1,0], rho_K[n-1,-1], rho_K[n-1,0])

        rho_K[n,1:-1] = rho_K[n-1,1:-1] + dt/lengthK * (FLUX_0[0][:-1] - FLUX_0[0][1:])
        rho_K[n,-1] = rho_K[n-1,-1] + dt/lengthK * (FLUX_0[0][-1] - FLUX_end[0])
        rho_K[n,0] = rho_K[n-1,0] + dt/lengthK * (FLUX_end[0] - FLUX_0[0][0])

        u_K[n,1:-1] = inverse_handle_zero_vec(rho_K[n,1:-1], tol_density_nullity) * (rho_K[n-1,1:-1]*u_K[n-1,1:-1] + dt/lengthK * (FLUX_0[1][:-1] - FLUX_0[1][1:]))
        u_K[n,-1] = inverse_handle_zero(rho_K[n,-1], tol_density_nullity) * (rho_K[n-1,-1]*u_K[n-1,-1] + dt/lengthK * (FLUX_0[1][-1] - FLUX_end[1]))
        u_K[n,0] = inverse_handle_zero(rho_K[n,0], tol_density_nullity) * (rho_K[n-1,0]*u_K[n-1,0] + dt/lengthK * (FLUX_end[1] - FLUX_0[1][0]))
    
    return (rho_K, u_K)

def num_flux_PGD_linear(u_i, u_i_1, rho_i, rho_i_1, slope_rho_i, slope_rho_i_1):
    rho_i_d, rho_i_1_g = rho_i + slope_rho_i*lengthK/2, rho_i_1 - slope_rho_i_1*lengthK/2
    if u_i > 0 and u_i_1 >= 0:
        return (u_i*rho_i_d, u_i**2*rho_i_d)
    elif u_i > 0 and u_i_1 < 0:
        return (u_i*rho_i_d + u_i_1*rho_i_1_g, u_i**2*rho_i_d + u_i_1**2*rho_i_1_g)
    elif u_i <= 0 and u_i_1 >= 0:
        return (0, 0)
    elif u_i <= 0 and u_i_1 < 0:
        return (u_i_1*rho_i_1_g, u_i_1**2*rho_i_1_g)

num_flux_PGD_linear_vec = np.vectorize(num_flux_PGD_linear)

def slope_limiter(r, type="vanLeer"):
    if type == "vanLeer":
        return 2*(r + abs(r)) / (1 + r)**2
    if type == "vanAlbada":
        return 2*r / (1 + r**2)
    if type == "superbee":
        return max(0, min(4*r/(1+r), 2/(1+r)), min(2*r/(1+r), 4/(1+r)))
    else:
        return 0
vec_slope_limiter = np.vectorize(slope_limiter, excluded=['type'])

def compute_r(u_i__1, u_i, u_i_1):
    if u_i - u_i__1 < 1e-8:
        return 0
    return (u_i_1 - u_i) / (u_i - u_i__1)
vec_compute_r = np.vectorize(compute_r)

"""
def pgd_compute_slope(rho_i__1, rho_i, rho_i_1, slope_limit=50):
    slope_1 = (rho_i_1 - rho_i__1)/lengthK
    if abs(slope_1) > slope_limit:
        if slope_1 > 0: slope_1 = slope_limit
        else: slope_1 = -slope_limit
    if rho_i - slope_1*lengthK/2 >= 0 and rho_i + slope_1*lengthK/2 >= 0:
        return slope_1
    elif slope_1 >= 0:
        return rho_i/(lengthK/2)
    else:
        return -rho_i/(lengthK/2)
pgd_compute_slope_vec = np.vectorize(pgd_compute_slope, excluded=['slope_limit'])
"""

def pgd_compute_slope(rho_i__1, rho_i, rho_i_1, slope_limiter_type="superbee"):
    slope_1 = (rho_i_1 - rho_i__1)/lengthK/2
    return slope_limiter(compute_r(rho_i__1, rho_i, rho_i_1), type=slope_limiter_type) * slope_1
pgd_compute_slope_vec = np.vectorize(pgd_compute_slope, excluded=['slope_limiter_type'])

def linear_finite_vol_for_pressureless_gas_dynamics():
    # rho_ini_K = ini()
    # rho_ini_K = 0.001*np.ones((nK,)) + ini_step()
    # rho_ini_K = ini_step()
    # u_ini_K = np.ones((nK,))

    rho_ini_K, u_ini_K = ini_chock()
    rho_ini_K, u_ini_K = ini_chock_steps()


    rho_K = np.zeros((nt, nK))
    rho_K[0,:] = rho_ini_K
    u_K = np.zeros((nt, nK))
    u_K[0,:] = u_ini_K

    tol_density_nullity = 1e-8

    slope_K = np.zeros((nt, nK))
    slope_K[0,1:-1] = pgd_compute_slope_vec(rho_K[0,:-2], rho_K[0,1:-1], rho_K[0,2:])
    slope_K[0,0] = pgd_compute_slope(rho_K[0,-1], rho_K[0,0], rho_K[0,1])
    slope_K[0,-1] = pgd_compute_slope(rho_K[0,-2], rho_K[0,-1], rho_K[0,0])

    FLUX_0 = (np.zeros((nK-1,)), np.zeros((nK-1,)))
    FLUX_end = (0., 0.)

    for n in range(1, nt):
        # print(n)
        FLUX_0 = num_flux_PGD_linear_vec(u_K[n-1,:-1], u_K[n-1,1:], rho_K[n-1,:-1], rho_K[n-1,1:], slope_K[n-1,:-1], slope_K[n-1,1:])
        FLUX_end = num_flux_PGD_linear(u_K[n-1,-1], u_K[n-1,0], rho_K[n-1,-1], rho_K[n-1,0], slope_K[n-1,-1], slope_K[n-1,0])

        rho_K[n,1:-1] = rho_K[n-1,1:-1] + dt/lengthK * (FLUX_0[0][:-1] - FLUX_0[0][1:])
        rho_K[n,-1] = rho_K[n-1,-1] + dt/lengthK * (FLUX_0[0][-1] - FLUX_end[0])
        rho_K[n,0] = rho_K[n-1,0] + dt/lengthK * (FLUX_end[0] - FLUX_0[0][0])

        u_K[n,1:-1] = inverse_handle_zero_vec(rho_K[n,1:-1], tol_density_nullity) * (rho_K[n-1,1:-1]*u_K[n-1,1:-1] + dt/lengthK * (FLUX_0[1][:-1] - FLUX_0[1][1:]))
        u_K[n,-1] = inverse_handle_zero(rho_K[n,-1], tol_density_nullity) * (rho_K[n-1,-1]*u_K[n-1,-1] + dt/lengthK * (FLUX_0[1][-1] - FLUX_end[1]))
        u_K[n,0] = inverse_handle_zero(rho_K[n,0], tol_density_nullity) * (rho_K[n-1,0]*u_K[n-1,0] + dt/lengthK * (FLUX_end[1] - FLUX_0[1][0]))

        slope_K[n,1:-1] = pgd_compute_slope_vec(rho_K[n,:-2], rho_K[n,1:-1], rho_K[n,2:])
        slope_K[n,0] = pgd_compute_slope(rho_K[n,-1], rho_K[n,0], rho_K[n,1])
        slope_K[n,-1] = pgd_compute_slope(rho_K[n,-2], rho_K[n,-1], rho_K[n,0])
    
    return (rho_K, u_K)


# rho_K_advection = finite_vol_for_advection()
# ani1 = plot_rho(rho_K_advection, X[:-1])
# ani1.save("advection_step1.gif")

# rho_K_linear_advection = linear_finite_vol_advection()
# ani11 = plot_rho(rho_K_linear_advection, X[:-1])
# ani11.save("advection_linear7.gif")


# rho_K_PGD, u_K_PGD = finite_vol_for_pressureless_gas_dynamics()
# # ani2 = plot_rho(rho_K_PGD, X[:-1], limits_override=[0, 15])
# ani2 = plot_rho(rho_K_PGD, X[:-1])
# ani2.save("pgd_step5.gif")
# ani3 = plot_u(u_K_PGD, X[:-1])
# ani3.save("pgd_step5_u.gif")

rho_K_linear_PGD, u_K_linear_PGD = linear_finite_vol_for_pressureless_gas_dynamics()
ani4 = plot_rho(rho_K_linear_PGD, X[:-1])
ani4.save("pgd_linear_shock3.gif")
ani5 = plot_u(u_K_linear_PGD, X[:-1])
ani5.save("pgd_linear_skock3_u.gif")

